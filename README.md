# ics-ans-role-node-red

Ansible role to install Node-RED.

## Role Variables

```yaml
node_red_nodejs_version: 10.9.0
node_red_version: 0.20.8
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-node-red
```

## License

BSD 2-clause
