import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_node_red_installed(host):
    cmd = host.run("node-red --help")
    assert cmd.rc == 0
    assert "Usage: node-red" in cmd.stdout
